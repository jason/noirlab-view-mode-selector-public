# NOIRLab View Mode Selector Drupal 8 Module

## Purpose

Fix the module View Mode Selector.

View Mode Selector will not change the view mode during template render. It's functionality is limited to usage with referenced entities.

## Usage

1. On your target content type create a new field with the machine name ```field_view_mode``` of the type ```View Mode Selector```.

2. Edit a node and select the ```View Mode```.

## Example

For the NOIRLab Intranet it was requested that a Page content type only show the body content. The content type was using the default template which shows all fields which included images and files.

This module was used to set the ```View Mode``` on the node to a custom View Mode named ```Body Only```.